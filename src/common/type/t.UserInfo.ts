export type TUserInfo = {
  email: string;
  fullName: string;
}
