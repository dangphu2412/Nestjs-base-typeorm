export type TJwtPayload = {
  userId: number;
  permissions: string[];
}
